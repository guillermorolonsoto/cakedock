# CakeDock

CakeDock is a CakePHP 2.x environment for Docker.
Requirements:

* Docker.
* Docker-Compose.

Includes:

* Apache 2.
* PHP 5.6 + extensions.
* Memcached.
* MySQL 5.7.
* Wkhtmltopdf.

## Getting started
---
1. Clone your project repository and configure.
2. Clone CakeDock inside your project.
   ``` $ git clone https://guillermorolonsoto@bitbucket.org/guillermorolonsoto/cakedock.git ```
3. Copy the *example.env* file with name *.env*.
    ```$ cp example.env .env```
4. Configure your *.env* file with your preferences.
5. Enjoy a complete environment for your project and a clean personal computer.


---
## Configurations
In your *.env* file you may configure the follow options:
+ WORKSPACE
```
    DOCUMENT_ROOT=../
```

By default, the CakeDock folder should be include inside in you project folder. But you may configure with above line.

+ APACHE
```
    APACHE_PORT_HTTP=80
    APACHE_PORT_HTTPS=443
```
In this section, you may configure the apache ports forwarding. Then, you may to access with your HTTP port in your localhost. For example, if you change the HTTP port to 8080, in your browser will access:
```
    http://localhost:8080
```

+ MYSQL

If you've a local database in MySQL, is recommended to change the port to forwarding.

```
MYSQL_PORT=3306
MYSQL_DATA_DIR=~/.cakedock/data/mysql
MYSQL_LOG_DIR=~/.cakedock/logs/mysql
MYSQL_DATABASE=db
MYSQL_USER=user
MYSQL_PASSWORD=secret
MYSQL_ROOT_PASSWORD=root
```

+ MEMCACHED

```
MEMCACHED_PORT=11211
```

## New in Docker?
---
### Up your services
In cakedock directory, type:
    
    $ docker-compose up -d mysql workspace

In the *.yml* file observe that **memcached service is linked to workspace service**. Therefore, is not necessary to include this service to up.
> * Why MySQL is not linked?
>
>Because if you prefer to use your local database config, >shouldn't obligate to up this service.

### Down your services
In cakedock directory, type:

    $ docker-compose down

## Suggestions?
Of course, I'm new and I learned docker 2 days ago. I started this project in 1 day. For suggestions and corrections please leave me a pull request. Thanks.